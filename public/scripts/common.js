(function($){
    $.fn.extend({
        formatInput: function(settings) {
            let $elem = $(this);
            settings = $.extend({
                    errback: null
            }, settings);
            $elem.bind("keyup.filter_input", $.fn.formatEvent);
        },
        formatEvent: function(e) {
            let elem = $(this);
            let initial_value = elem.val();
            elem.val($.fn.insertSpaces(initial_value));
        },
        insertSpaces: function(number) {
            return number.replace(' ', '').replace(/\B(?=(?:\d{3})+(?!\d))/g, ' ');
        }
    });
})(jQuery);

const dictionaryStringErrors = {
    errCode1: 'Неправильный номер карты отправителя',
    errCode2: 'Неправильный срок действия карты отправителя',
    errCode3: 'Неправильный номер карты получателя'
}

function makeFetch__JSON__POST(url, data, cb) {
    var contentHeaders = new Headers({'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'});
    fetch(url, {
        method: 'POST',
        dataType: "text",
        headers: contentHeaders,
        body: data
    }).then((response) => {
        return response;
    }).then((res) => {
        if (cb instanceof Function) {
            cb(res);
        }
    });
}

class App {
    constructor(settings) {
        const app = this;   
        app.validation = {
            cardSenderNumber : false,
            cardRecipientNumber : false,
            cardSenderValidity : false
        };     

        app.$tpl = $('.container');

        app.rendererMainPage();
    }

    rendererMainPage(test) {
        let _this = this;

        let $target = this.$tpl;

        this.$tpl.find('.amount').formatInput();

        _this.cardSender = new Card({
            renderTarget: `.card-container`,
            type: `Sender`,
            clazz: `card f-left m-left`,
            paymentSystems: [`MIR`, `Maestro`,`MasterCard`, `Visa`],
            onFirstSegmentReady(segment) {
                var cardInfo = new CardInfo(segment);

                _this.cardSender.updatePaymentSystem({
                    bankData: {                        
                        paymentSystem: cardInfo.brandName ? [cardInfo.brandName] : [`MIR`, `Maestro`,`MasterCard`, `Visa`]
                    }
                });
            },
            onSecondSegmentReady(segment) {
                var cardInfo = new CardInfo(segment);

                _this.cardSender.updateBank({
                    bankData: {
                        bank: cardInfo.bankNameEn,
                        theme: cardInfo.backgroundLightness,
                        backgroundColors: cardInfo.backgroundColors
                    }
                });   
            },
            onLastSymbol() {
                _this.showError('errCode1');
            },
            onRemoveLastSymbol() {
                _this.hideError('errCode1');
            },
            onLastDateSymbol() {
                _this.showError('errCode2');
            },
            onRemoveLastDateSymbol() {
                _this.hideError('errCode2');
            },
            onCardNumberValid(cardNumberValidity) {
                _this.validation.cardSenderNumber = cardNumberValidity;
            },
            onDateValid(dateValidity){
                _this.validation.cardSenderValidity = dateValidity;
            }

        });

        _this.cardRecipient = new Card({
            renderTarget: `.card-container`,
            type: `Recipient`,
            clazz: `card f-right m-right`,
            paymentSystems: [`MIR`, `Maestro`,`MasterCard`, `Visa`],
            onFirstSegmentReady(segment) {
                var cardInfo = new CardInfo(segment);
                
                _this.cardRecipient.updatePaymentSystem({
                    bankData: {
                        paymentSystem: cardInfo.brandName ? [cardInfo.brandName] : [`MIR`, `Maestro`,`MasterCard`, `Visa`]
                    }
                });
            },
            onSecondSegmentReady(segment) {
                var cardInfo = new CardInfo(segment);
                
                _this.cardRecipient.updateBank({
                    bankData: {
                        bank: cardInfo.bankNameEn,
                        theme: cardInfo.backgroundLightness,
                        backgroundColors: cardInfo.backgroundColors
                    }
                });
            },
            onLastSymbol() {
                _this.showError('errCode3');
            },
            onRemoveLastSymbol() {
                _this.hideError('errCode3');
            },
            onCardNumberValid(cardNumberValidity) {
                _this.validation.cardRecipientNumber = cardNumberValidity;
            }
        });
        
        this.$tpl.find('.amount').on("keyup", function (event) {           
            let $t = $(this);

            let amountValue = parseInt($t.val().replace(/\s/g, ''));

            if (
                ((event.which < 48 || event.which > 57) && 
                (event.which < 96 || event.which > 105) && 
                (event.which !== 8) && (event.which !== 0) && 
                (event.which === 32)) ||
                amountValue >= 100000
            ) {                
                event.preventDefault();
            } 

            if (amountValue && _this.checkValidation()) {
                makeFetch__JSON__POST('https://www.alfaportal.ru/card2card/fee/alfaportal/', 
                    "tfr_card_src_num=" + _this.cardSender.getCardNumber() + 
                    "&tfr_card_dst_num=" + _this.cardRecipient.getCardNumber() +
                    "&tfr_amount=" + String(amountValue) +
                    "&tfr_currency=810"
                , (res) => {
                    if (res) { 
                        _this.$tpl.find('.comission').text(`Комиссия —  ${res} Р`);
                    }  
                });                     
            }

            if (event.which === 8 && amountValue.length === 0){
                _this.$tpl.find('.comission').text('');
            }
        });

        this.$tpl.find('.amount').on("paste", function (event) {
            let $t = $(this);
            let clipboardData = event.clipboardData || event.originalEvent.clipboardData || window.clipboardData;
            let pastedData = clipboardData.getData('text');

            $t.val('');    

            setTimeout(() => {
                pastedData = pastedData.replace(/[^\d]/g, '').replace(/\B(?=(?:\d{3})+(?!\d))/g, '$1 ').trim();
                $t.val(pastedData);
            });         

            return true;
        });

         this.$tpl.find('.send-button').click(function () {
                if ($(this).hasClass('active')) {
                    let result = _this.collectSendData();
                    _this.$tpl.find('#tfr_card_src_exp_month').val(result['tfr_card_src_exp_month']);                        
                    _this.$tpl.find('#tfr_card_src_exp_year').val(result['tfr_card_src_exp_year']);                        
                    _this.$tpl.find('#tfr_card_src_num').val(result['tfr_card_src_num']);                        
                    _this.$tpl.find('#tfr_card_cvv').val(result['tfr_card_cvv']);                        
                    _this.$tpl.find('#tfr_card_dst_num').val(result['tfr_card_dst_num']);                        
                    _this.$tpl.find('#tfr_amount').val(result['tfr_amount']);                        
                    _this.$tpl.find('#tfr_currency').val(result['tfr_currency']);  
                    
                }
        }); 

        this.$tpl.filter('.container').on('keyup', (event) => {
            if (_this.checkValidation() && _this.$tpl.find('.amount').val().length) {
                this.$tpl.find('.send-button').removeClass('active').addClass('active');
            } else {
                this.$tpl.find('.send-button').removeClass('active');
            }
        });
    }

    showError(errorCode) {
        this.$tpl.find('.error-message').append(`<div class="${errorCode}">${dictionaryStringErrors[errorCode]}</div>`).css({'display': 'block'});
    }

    hideError(errorCode) {
        this.$tpl.find(`.${errorCode}`).remove();
    }

    checkValidation() {
        return this.validation.cardSenderNumber && this.validation.cardRecipientNumber && this.validation.cardSenderValidity;
    }

    collectSendData() {
        return {
            "tfr_card_src_exp_month" : this.cardSender.getValidity().slice(0,2),
            "tfr_card_src_exp_year" : this.cardSender.getValidity().slice(3,5),
            "tfr_card_src_num" : this.cardSender.getCardNumber(),
            "tfr_card_cvv" : this.cardSender.getCVC(),
            "tfr_card_dst_num" : this.cardRecipient.getCardNumber(),
            "tfr_amount" : this.$tpl.find('.amount').val().replace(/\s/g, ''),
            "tfr_currency" : 810
        }
    }
}

$(() => {
    let app = new App({});   
    
    let cleaveSender = new Cleave('.card-number-Sender', {
        creditCard: true
    }); 

    let cleaveRecipient = new Cleave('.card-number-Recipient', {
        creditCard: true      
    });    
});