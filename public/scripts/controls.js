class Control {
    constructor(settings) {
        this.settings = settings;
    }

    renderer($tpl) {
        let $target = $(this.settings.renderTarget || `body`);
        $tpl.addClass(`control`);

        if (this.settings.clazz) {
            $tpl.addClass(this.settings.clazz);
        }

        $target.append($tpl);
    }
}

const makePaymentSystemTemplate = Symbol('makePaymentSystemTemplate');
const luhnValidation = Symbol('luhnValidation');
const dateValidation = Symbol('dateValidation');
class Card extends Control {
    constructor(settings) {
        super(settings);
        const control = this;
        control.settings = settings;

        control.$tpl = $(`
            <div class="c-container">
                <div class="card-bank-logo"></div>
                <div class="payment-system-container"></div>
                <div class="input-container">
                    <div class="card-number-input">
                        <span class="card-number-info info hide">Номер карты</span>
                        <input type="text" class="card-number card-number-${control.settings.type}" placeholder="Номер карты">
                    </div>                    
                </div> 
                <div class="focus-box-shadow hide"></div> 
            </div>
        `);

        if(control.settings.type == 'Sender') {
            control.$tpl.find('.input-container').append(`
                <div class="date-input"> 
                    <span class="date-info info hide">ММ/ГГ</span>              
                    <input type="text" maxlength="5" class="date" placeholder="ММ/ГГ">
                </div>
                <div class="cvc-input">
                    <span class="cvc-info info hide">CVC</span>
                    <input type="password" maxlength="3" class="cvc" placeholder="CVC">
                </div>
                
            `);
        }
        
        control.updateInputBlock('.card-number');
        control.updateInputBlock('.cvc');
        control.updateInputBlock('.date');        

        control.$tpl.find('.card-number').on("keypress", function (event) {
            let $t = $(this);

            let cardNumber = $t.val().replace(/ /g, '');            

            if (cardNumber.length < 16) {

                if(cardNumber.length === 3) {
                    control.settings.onFirstSegmentReady(($t[0].value + event.key).toString());
                }
            } 

            return true;
        });

        control.$tpl.find('.card-number').on("keydown", function (event) {
            let $t = $(this);     

            if (event.which === 8) {

                if($t[0].value.length === 5) {
                    control.settings.onFirstSegmentReady(0);                    
                }

                if ($t[0].value.length === 19) {
                    control.settings.onRemoveLastSymbol();
                    control.settings.onCardNumberValid(false);
                }
            }

            if ($t[0].value.length === 18 && event.which !== 8) {
                if (!control[luhnValidation](($t[0].value + event.key).replace(/\s/g,'').toString())) {
                    control.settings.onLastSymbol();
                }

                control.settings.onCardNumberValid(control[luhnValidation](($t[0].value + event.key).replace(/\s/g,'').toString()));
            }        
        });

        control.$tpl.find('.card-number').on("paste", function (event) {
            
            let $t = $(this);            
            let clipboardData = event.clipboardData || event.originalEvent.clipboardData || window.clipboardData;
            let pastedData = clipboardData.getData('text');

            $t.val('');    

            setTimeout(() => {
                pastedData = pastedData.replace(/[^\d]/g, '').replace(/(.{4})/g, '$1 ').trim();
                $t.val(pastedData);
                
                control.settings.onFirstSegmentReady($t.val().toString()); 

                if ($t.val().length == 19) {
                    if (!control[luhnValidation](($t.val()).replace(/\s/g,'').toString())) {
                        control.settings.onLastSymbol();
                    }
                    control.settings.onCardNumberValid(control[luhnValidation](($t.val()).replace(/\s/g,'').toString()));
                }
            });         

            return true;
        });


        control.$tpl.find('.date').on("keydown", function (event) {
            let $t = $(this); 
            if (
                (event.which < 48 || event.which > 57) && 
                (event.which < 96 || event.which > 105) && 
                (event.which !== 8) && 
                (event.which !== 0) &&
                (event.which !== 9)
            ) {
                event.preventDefault();
            } 

            if(event.which !== 8) {
                if ($t[0].value.length === 2) {
                    $t[0].value += '/';
                }
                if($t[0].value.length === 4) { 
                    control.settings.onDateValid(!control[dateValidation](($t[0].value + event.key).replace('/', ''))); 

                    if(control[dateValidation](($t[0].value + event.key).replace('/', ''))) {
                        control.settings.onLastDateSymbol(); 
                    }                    
                }
            }

            if (event.which === 8) {
                if ($t[0].value[$t[0].value.length - 2] === '/') {
                    $t[0].value = $t[0].value.slice(0, -1);
                }
                if ($t[0].value.length === 5) {
                    control.settings.onRemoveLastDateSymbol();
                    control.settings.onDateValid(false);  
                }
            }
            return true;            
        });

        control.$tpl.find('.cvc').on("keydown", function (event) {
            let $t = $(this); 
            if (
                (event.which < 48 || event.which > 57) && 
                (event.which < 96 || event.which > 105) && 
                (event.which !== 8) && 
                (event.which !== 0) &&
                (event.which !== 9)
            ) {
                event.preventDefault();
            }           
        });

        control.update();
        control.renderer(control.$tpl);
    }

    [makePaymentSystemTemplate](PSList) {
        if (PSList) {
            let $PSTpl = $(`<div></div>`);
            for (let i = 0; i < PSList.length; i++) {
                $PSTpl.append(`<span class="payment-system ${PSList[i]}"></span>`);
            }
            $PSTpl.append(`<div class="clear"></div>`);

            return $PSTpl;
        } else {
            return false;
        }
    }

    [luhnValidation](card_number) {
        var arr = [];

        for(var i = 0; i < card_number.length; i++) {
            if(i % 2 === 0) {
                var m = parseInt(card_number[i]) * 2;
                if(m > 9) {
                    arr.push(m - 9);
                } else {
                    arr.push(m);
                } 
            } else {
                var n = parseInt(card_number[i]);
                arr.push(n)
            }
        }
        var summ = arr.reduce(function(a, b) { return a + b; });
        return (!(summ % 10));
    }

    [dateValidation](date) {
        var month = date.slice(0, 2);
        var year = date.slice(2, 4);
        if (month <= 0 || month > 12) {
            return true;
        }
        
        var currentDate = new Date();
        var currentMonth = currentDate.getMonth();
        var currentYear = currentDate.getFullYear().toString().slice(2, 4);
        if (year < currentYear || year > parseInt(currentYear) + 10) {
            return true;
        }
        if (currentYear === year) {
            if (currentMonth > month) {
                return true;
            }
        } 
        if (parseInt(currentYear) + 10 == year) {
            if (currentMonth < month) {
                return true;
            }
        } 
        return false;
    }

    update() {
        this.$tpl.find('.payment-system').remove();
        this.$tpl.find('.payment-system-container').append(this[makePaymentSystemTemplate](this.settings.paymentSystems));
        this.$tpl.find('.card-bank-logo').text(this.settings.bank);

        if (this.settings.bank) {
            if (this.settings.bank[0] != 'unknown') {
                this.$tpl.filter('.c-container').removeClass('light dark');
                this.$tpl.filter('.c-container').addClass(this.settings.theme);
                
                this.$tpl.filter('.card').css({
                    'background': 'linear-gradient(to top right, ' + this.settings.backgroundColors[0] + ', ' + this.settings.backgroundColors[1] + ')' 
                });                              
            }
        } 
    }

    updatePaymentSystem(params) {
        this.settings.paymentSystems = params.bankData.paymentSystem;
        this.update();
    }

    updateBank(params) {
        this.settings.bank = params.bankData.bank;
        this.settings.theme = params.bankData.theme;
        this.settings.backgroundColors = params.bankData.backgroundColors;
        this.update();
    }

    updateValidationMessages(params) {
        this.update();
    }

    getCardNumber() {
        return this.$tpl.find('.card-number').val();
    }

    getValidity() {
        return this.$tpl.find('.date').val();
    }

    getCVC() {
        return this.$tpl.find('.cvc').val();
    }
    
    setCardNumber(val) {
        let splittedValue = val.split(' ');
        this.$tpl.find('.card-number').val(val);
        this.settings.onFirstSegmentReady(splittedValue[0]);
        this.settings.onSecondSegmentReady(splittedValue[1]);
    }

    setValidity(val) {
        this.$tpl.find('.date').val(val);
    }

    setCVC(val) {
        this.$tpl.find('.cvc').val(val);
    }

    updateInputBlock(target) {
        this.$tpl.find(target).on("focus", () => {
            this.$tpl.find(target + '-info').removeClass('visible hide').addClass('visible');
            if (target === '.card-number') {
                this.$tpl.find(target).attr('placeholder', '0000 0000 0000 0000');
            }
            
            this.$tpl.find('.focus-box-shadow').removeClass('visible hide').addClass('visible');
        });

        this.$tpl.find(target).on("blur", () => {
            if (this.$tpl.find(target)[0].value === "") {
                this.$tpl.find(target + '-info').removeClass('visible hide').addClass('hide');
                this.$tpl.find('.focus-box-shadow').removeClass('visible hide').addClass('hide');
            }            
            
            if (target === '.card-number') {
                this.$tpl.find(target).attr('placeholder', 'Номер карты');
            }
        });
    } 
}