const express = require('express');
const path = require('path');
const _ = require('lodash');
const bodyParser = require('body-parser');
const port = 4422;

var app = express();

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.listen(port, function() {
    console.log('The server up on port: ' + port);
});

app.post('/get-payment-system/', (req, res) => {
    let ret = { };

    switch(req.body.segment) {
        case '2222': ret.paymentSystem = 'MasterCard'; break; 
        case '4444': ret.paymentSystem = 'Maestro'; break; 
        default: ret.paymentSystem = 'Unknown payment system'; break; 
    }

    res.send(ret);
});

app.post('/get-bank/', (req, res) => {
    let ret = { };
    
    switch(req.body.segment) {
        case '8888': 
            ret.bank = 'sberbank'; 
            ret.theme = "dark";
        break; 
        case '0000': 
            ret.bank = 'raiffeisen';
            ret.theme = "light";
        break; 
        default: ret.bank = 'unknown'; 
        break; 
    }

    res.send(ret);
});

app.post('/get-card-info/', (req, res) => {
    let ret = { };
    
    switch(req.body.card) {
        case '2222 8888 2222 8888': 
            ret.validity = '12/12';
        break; 
        case '0000 4444 0000 4444': 
            ret.validity = '11/11';
        break; 
        default: ret.validity = 'unknown'; 
        break; 
    }

    res.send(ret);
});

app.get('*', function(req, res) {
    res.sendStatus(404);
});